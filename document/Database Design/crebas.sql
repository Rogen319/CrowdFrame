/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     2017/7/14 14:47:03                           */
/*==============================================================*/


drop table if exists ActionInput;

drop table if exists EnumOutput;

drop table if exists InfoObject;

drop table if exists LocAction;

drop table if exists Location;

drop table if exists NumericalOutput;

drop table if exists PhysicalObject;

drop table if exists PictureOutput;

drop table if exists Stage;

drop table if exists Task;

drop table if exists Template;

drop table if exists TextOutput;

drop table if exists User;

drop table if exists application;

drop table if exists collect;

drop table if exists reserve;

drop table if exists undertake;

/*==============================================================*/
/* Table: ActionInput                                           */
/*==============================================================*/
create table ActionInput
(
   inputId              int not null auto_increment,
   actionId             int not null,
   inputType            int,
   inputDesc            varchar(1024),
   inputValue           varchar(1024),
   primary key (inputId)
);

/*==============================================================*/
/* Table: EnumOutput                                            */
/*==============================================================*/
create table EnumOutput
(
   outputId             int not null auto_increment,
   actionId             int not null,
   outputDesc           varchar(1024),
   outputValue          varchar(1024),
   workerId             int,
   outputIndicator      int,
   entries              varchar(1024),
   enumAggregate        int,
   primary key (outputId)
);

/*==============================================================*/
/* Table: InfoObject                                            */
/*==============================================================*/
create table InfoObject
(
   objectId             int not null auto_increment,
   stageId              int,
   actionId             int not null,
   primary key (objectId)
);

/*==============================================================*/
/* Table: LocAction                                             */
/*==============================================================*/
create table LocAction
(
   actionId             int not null auto_increment,
   locationId           int not null,
   duration             float,
   actionType           int,
   primary key (actionId)
);

/*==============================================================*/
/* Table: Location                                              */
/*==============================================================*/
create table Location
(
   locationId           int not null auto_increment,
   stageId              int not null,
   address              varchar(1024),
   latitude             float,
   longitude            float,
   type                 int,
   primary key (locationId)
);

/*==============================================================*/
/* Table: NumericalOutput                                       */
/*==============================================================*/
create table NumericalOutput
(
   outputId             int not null auto_increment,
   actionId             int not null,
   outputDesc           varchar(1024),
   outputValue          varchar(1024),
   workerId             int,
   outputIndicator      int,
   intervalIndicator    int,
   upBound              float,
   lowBound             float,
   numberAggregate      int,
   primary key (outputId)
);

/*==============================================================*/
/* Table: PhysicalObject                                        */
/*==============================================================*/
create table PhysicalObject
(
   objectId             int not null auto_increment,
   stageId              int,
   actionId             int not null,
   primary key (objectId)
);

/*==============================================================*/
/* Table: PictureOutput                                         */
/*==============================================================*/
create table PictureOutput
(
   outputId             int not null auto_increment,
   actionId             int not null,
   outputDesc           varchar(1024),
   outputValue          varchar(1024),
   workerId             int,
   outputIndicator      int,
   primary key (outputId)
);

/*==============================================================*/
/* Table: Stage                                                 */
/*==============================================================*/
create table Stage
(
   stageId              int not null auto_increment,
   taskId               int not null,
   stageName            varchar(1024),
   stageDesc            varchar(1024),
   deadline             timestamp,
   reward               float,
   stageIndex           int,
   workerNum            int,
   aggregateMethod      int,
   aggregateResult      varchar(1024),
   restrictions         bigint,
   stageDuration        float,
   stageContract        timestamp,
   stageStatus          int,
   primary key (stageId)
);

/*==============================================================*/
/* Table: Task                                                  */
/*==============================================================*/
create table Task
(
   taskId               int not null auto_increment,
   templateId           int not null,
   userId               int not null,
   title                varchar(1024),
   description          varchar(1024),
   taskStatus           int,
   currentStage         int,
   bonus                float,
   publish_time         timestamp,
   ddl                  timestamp,
   userScope            int,
   primary key (taskId)
);

/*==============================================================*/
/* Table: Template                                              */
/*==============================================================*/
create table Template
(
   templateId           int not null auto_increment,
   userId               int not null,
   tempName             varchar(1024),
   tempDesc             varchar(1024),
   heat                 int,
   uri                  varchar(1024),
   create_time          timestamp,
   stageNum             int,
   primary key (templateId)
);

/*==============================================================*/
/* Table: TextOutput                                            */
/*==============================================================*/
create table TextOutput
(
   outputId             int not null auto_increment,
   actionId             int not null,
   outputDesc           varchar(1024),
   outputValue          varchar(1024),
   workerId             int,
   outputIndicator      int,
   primary key (outputId)
);

/*==============================================================*/
/* Table: User                                                  */
/*==============================================================*/
create table User
(
   userId               int not null auto_increment,
   username             varchar(1024),
   password             varchar(1024),
   creditPublish        float,
   creditWithdraw       float,
   headImg              varchar(1024),
   userTag              int,
   loginIndicator       int,
   weixinId             varchar(1024),
   phone                varchar(15),
   primary key (userId)
);

/*==============================================================*/
/* Table: application                                           */
/*==============================================================*/
create table application
(
   applicationId        int not null auto_increment,
   userId               int not null,
   stageId              int not null,
   primary key (applicationId)
);

/*==============================================================*/
/* Table: collect                                               */
/*==============================================================*/
create table collect
(
   collectId            int not null auto_increment,
   userId               int not null,
   templateId           int not null,
   primary key (collectId)
);

/*==============================================================*/
/* Table: reserve                                               */
/*==============================================================*/
create table reserve
(
   reserveId            int not null auto_increment,
   userId               int not null,
   stageId              int not null,
   reserveTime          timestamp,
   previousUserId       int,
   previousStageId      int,
   reserveStatus        int,
   reserveContract      timestamp,
   primary key (reserveId)
);

/*==============================================================*/
/* Table: undertake                                             */
/*==============================================================*/
create table undertake
(
   undertakeId          int not null auto_increment,
   userId               int not null,
   stageId              int not null,
   start_time           timestamp,
   end_time             timestamp,
   contract_time        timestamp,
   status               int,
   primary key (undertakeId)
);

alter table ActionInput add constraint FK_need foreign key (actionId)
      references LocAction (actionId) on delete restrict on update restrict;

alter table EnumOutput add constraint FK_generate foreign key (actionId)
      references LocAction (actionId) on delete restrict on update restrict;

alter table InfoObject add constraint FK_IO foreign key (actionId)
      references LocAction (actionId) on delete restrict on update restrict;

alter table InfoObject add constraint FK_aggregate foreign key (stageId)
      references Stage (stageId) on delete restrict on update restrict;

alter table LocAction add constraint FK_executeOn foreign key (locationId)
      references Location (locationId) on delete restrict on update restrict;

alter table Location add constraint FK_involve foreign key (stageId)
      references Stage (stageId) on delete restrict on update restrict;

alter table NumericalOutput add constraint FK_generate4 foreign key (actionId)
      references LocAction (actionId) on delete restrict on update restrict;

alter table PhysicalObject add constraint FK_IO2 foreign key (actionId)
      references LocAction (actionId) on delete restrict on update restrict;

alter table PhysicalObject add constraint FK_aggregate2 foreign key (stageId)
      references Stage (stageId) on delete restrict on update restrict;

alter table PictureOutput add constraint FK_generate3 foreign key (actionId)
      references LocAction (actionId) on delete restrict on update restrict;

alter table Stage add constraint FK_include foreign key (taskId)
      references Task (taskId) on delete restrict on update restrict;

alter table Task add constraint FK_instantiation foreign key (templateId)
      references Template (templateId) on delete restrict on update restrict;

alter table Task add constraint FK_publish foreign key (userId)
      references User (userId) on delete restrict on update restrict;

alter table Template add constraint FK_create foreign key (userId)
      references User (userId) on delete restrict on update restrict;

alter table TextOutput add constraint FK_generate2 foreign key (actionId)
      references LocAction (actionId) on delete restrict on update restrict;

alter table application add constraint FK_application foreign key (userId)
      references User (userId) on delete restrict on update restrict;

alter table application add constraint FK_application2 foreign key (stageId)
      references Stage (stageId) on delete restrict on update restrict;

alter table collect add constraint FK_collect foreign key (userId)
      references User (userId) on delete restrict on update restrict;

alter table collect add constraint FK_collect2 foreign key (templateId)
      references Template (templateId) on delete restrict on update restrict;

alter table reserve add constraint FK_reserve foreign key (userId)
      references User (userId) on delete restrict on update restrict;

alter table reserve add constraint FK_reserve2 foreign key (stageId)
      references Stage (stageId) on delete restrict on update restrict;

alter table undertake add constraint FK_undertake foreign key (stageId)
      references Stage (stageId) on delete restrict on update restrict;

alter table undertake add constraint FK_worker foreign key (userId)
      references User (userId) on delete restrict on update restrict;

