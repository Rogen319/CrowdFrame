delimiter $
CREATE EVENT updateExpiredOnStage
ON schedule every 5 minute
DO
BEGIN
update stage SET stageStatus = -1 where stageStatus = 0 and deadline < now();
update undertake SET status = -1 where status = 0 and contract_time < now();
END$
delimiter ; 