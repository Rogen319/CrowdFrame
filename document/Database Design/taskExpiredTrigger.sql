﻿delimiter $
create trigger taskExpired 
after update on stage 
for each row
begin
declare numOfStage,tid,uid int;
declare bonus,rewards double;
set tid = (select templateId from task where taskId=old.taskId);
set numOfStage = (select stageNum from template where templateId=tid);
set uid = (select userId from task where taskId=old.taskId);
set bonus = (select bonus from task where taskId=old.taskId);
set rewards = (select sum(reward) from stage where taskId=tid);
#update task set taskStatus=-1 where new.stageStatus=-1 and new.stageIndex=numOfStage and taskId=new.taskId;
#task只要有一个stage过期，那么就设置状态为过期
update task set taskStatus=-1 where new.stageStatus=-1 and taskId=new.taskId;
#当stage过期时,接受这个stage的undertake全部设为过期，不论是否完成
update undertake set status=-1 where new.stageStatus=-1 and stageId=new.stageId;
#返还任务发布者积分
update user set creditPublish=(creditPublish+bonus+rewards) where userId=uid;
end$
delimiter ;