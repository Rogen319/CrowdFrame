DROP EVENT IF exists updateLoginIndicator;
CREATE EVENT updateLoginIndicator
ON schedule every 1 day starts '2017-05-28 00:00:00'
DO
update user set loginIndicator=0;