CREATE DEFINER=`root`@`%` TRIGGER `taskExpired` AFTER UPDATE ON `stage`
FOR EACH ROW begin
declare numOfStage,tid,uid int;
#declare bonus,rewards,oldCredit,newCredit double;
set tid = (select templateId from task where taskId=old.taskId);
set numOfStage = (select stageNum from template where templateId=tid);
set uid = (select userId from task where taskId=old.taskId);
set @bonus = (select bonus from task where taskId=old.taskId);
set @rewards = (select sum(reward) from stage where taskId=old.taskId);
set @oldCredit = (select creditPublish from user where userId=uid);
set @newCredit = @oldCredit+@bonus+@rewards;
#update task set taskStatus=-1 where new.stageStatus=-1 and new.stageIndex=numOfStage and taskId=new.taskId;
#task只要有一个stage过期，那么就设置状态为过期
update task set taskStatus=-1 where new.stageStatus=-1 and taskId=new.taskId;
#当stage过期时,接受这个stage的undertake全部设为过期，不论是否完成
update undertake set status=-1 where new.stageStatus=-1 and stageId=new.stageId;
#返还任务发布者积分
update user set creditPublish=@newCredit where new.stageStatus=-1 and userId=uid;
end