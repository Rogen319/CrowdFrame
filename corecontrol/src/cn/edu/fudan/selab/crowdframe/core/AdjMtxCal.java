package cn.edu.fudan.selab.crowdframe.core;

import java.util.ArrayList;

/**
 * @Description In reference to http://bbs.csdn.net/topics/230031982
 * 			    给定邻接矩阵和起点终点，计算出所有可能的简单路径（不考虑有环的情况）
 * @author fzj
 * @date 2018年3月14日 上午9:23:11  
 * @version V1.0  
 */
public class AdjMtxCal {
    //有向带权图。-1表示无路可通。自己到自己也是-1。其它表示权值。
    public int[][] graph = null;
    public boolean[] isVisited = null;
    //true-表示该节点已访问过。false-表示还没有访问过。
     
    public ArrayList<String> res = null;
    //最后的所有的路径的结果。每一条路径的格式是如：0->2->1->3:7
    
    public AdjMtxCal(int[][] graph){
    		this.graph = graph;
    		isVisited = new boolean[graph.length];
    		res = new ArrayList<String>();
    }
     
    //求在图graph上源点s到目标点d之间所有的简单路径，并求出路径的和。   
    public  void getPaths(int s,int d,String path){
         isVisited[s]=true;//源点已访问过. 
	     for(int i=0; i<graph.length; i++){
	        if (graph[s][i]==-1 || isVisited[i]){continue;}
	        //若无路可通或已访问过，则找下一个结点。
	 
	        if(i==d){//若已找到一条路径        
	            res.add(path+"->"+d);//加入结果。
	            continue;         
	        }
	       
	        getPaths(i, d, path+"->"+i);//继续找       
	        isVisited[i]=false;       
	     }
    }
 
}