package cn.edu.fudan.selab.crowdframe.core;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import cn.edu.fudan.selab.crowdframe.util.Utils;

/**
 * @Description: TODO
 * @author fzj
 * @date 2018年3月14日 上午10:22:08  
 * @version V1.0  
 */
public class File2Dgraph {
	
	public static ListDgraph<String> readFromFile(String filename) {
		ListDgraph<String> ret = new ListDgraph<String>();
		try {
			
			String encoding = "UTF-8";
			File file = new File("topoFile/"+filename+".txt");
			if(file.isFile() && file.exists()) {
				InputStreamReader read = new InputStreamReader(new FileInputStream(file),encoding);
				BufferedReader bufferedReader = new BufferedReader(read);
				String line = null;
				while((line = bufferedReader.readLine())!=null) {
					System.out.println(line);
					if(!line.contains("->")) {
						System.out.println("line="+line);
						ret.add(line);
					}else {
						String[] array = line.split("->");
						//System.out.println("左边"+array[0]+"右边"+array[1]);
						ret.add(new Edge<String>(array[0], array[1]));
					}
				}
				read.close();
			}else {
				System.out.println("Read：找不到指定的文件.");
			}
			
		}catch(Exception e) {
			System.out.println("读取文件内容操作出错");
			e.printStackTrace();
		}
		
		return ret;
	}
     
	
	public static ArrayList<String> dgraph2matrix(ListDgraph<String> input,String root,String dest){
		ArrayList<String> ret = new ArrayList<String>();
		int[][] mat = null;
		int count = 0;
		HashMap<Integer, String> lookup = new HashMap<Integer,String>(); //用来对照地点名和编号
		Iterator<String> it = input.iterator(Dgraph.ITERATOR_TYPE_BFS, root);
	      while(it.hasNext()) {
	          String s = it.next();
	          Utils.log("next : %s", s);
	          lookup.put(count, s);
//	          pathList.add(s);
	    	  	 count++;
	    	
	      }
		System.out.println(count);
		
		mat = new int[count][count];
		//初始化矩阵
		for(int i = 0; i < count; i++) {
			for(int j = 0; j < count; j++) {
				if(input.get(i, j)!=null) {
					mat[i][j] = 1;
				}else {
					mat[i][j] = -1;
				}
			}
			
		}
		
		//查找root和dest对应的编号
		int rootid = 0;
		int destid = 0;
		Iterator iter = lookup.entrySet().iterator();
		while (iter.hasNext()) {
			Map.Entry entry = (Map.Entry) iter.next();
			int key =  (int) entry.getKey();
			String val = (String) entry.getValue();
			if(val.equals(root)) {
				rootid = key;
			}
			if(val.equals(dest)) {
				destid = key;
			}
		}
		
		 AdjMtxCal adjMtxCal = new AdjMtxCal(mat);
	       adjMtxCal.getPaths(rootid, destid, ""+rootid);
	       for(String e : adjMtxCal.res)//打印所有的结果
	       {
	    	   		String[] pStrings = e.split("->");
	    	   		String temp = "";
	            for(String string: pStrings) {
	            		//System.out.println("partstring="+string);
	            		temp += lookup.get(Integer.parseInt(string));
	            		temp += ">";
	            }
	            temp = temp.substring(0, temp.length()-1);
	            ret.add(temp);
	       }
		return ret;
	}
}
