package cn.edu.fudan.selab.crowdframe.util;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * @Description: TODO
 * @author fzj
 * @date 2018年3月13日 上午9:37:38  
 * @version V1.0  
 */
public class Utils {
	/**
     * 打印信息
     * @param t
     */
    public static void log(Object t) {
        System.out.println(t);
    }
    
    /**
     * 打印信息
     * @param t
     */
    public static void log(String format, Object... args) {
        String str = String.format(format, args);
        System.out.println(str);
    }
    
    
    /**
     * 打印出String类型的ArrayList中的所有元素
     * @param t
     */
    public static void printStringlist(ArrayList<String> list) {
    		System.out.println("====================== Now printing the elements of the arraylist");
    		Iterator it1 = list.iterator();  
        while (it1.hasNext()) {  
            System.out.println(it1.next());  
        }  
    }
    
}
