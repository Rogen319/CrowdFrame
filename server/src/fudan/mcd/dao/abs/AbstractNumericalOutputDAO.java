package fudan.mcd.dao.abs;

import javax.servlet.ServletContext;

import fudan.mcd.vo.NumericalOutputVO;

public abstract class AbstractNumericalOutputDAO extends AbstractOutputDAO<NumericalOutputVO> {
	public AbstractNumericalOutputDAO(ServletContext context) {
		super(context);
	}

	public AbstractNumericalOutputDAO(String configPath) {
		super(configPath);
	}
}
