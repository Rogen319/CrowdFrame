package fudan.mcd.dao.abs;

import javax.servlet.ServletContext;

import fudan.mcd.vo.TextOutputVO;

public abstract class AbstractTextOutputDAO extends AbstractOutputDAO<TextOutputVO> {
	public AbstractTextOutputDAO(ServletContext context) {
		super(context);
	}

	public AbstractTextOutputDAO(String configPath) {
		super(configPath);
	}
}
