package fudan.mcd.dao.abs;

import javax.servlet.ServletContext;

import fudan.mcd.vo.PictureOutputVO;

public abstract class AbstractPictureOutputDAO extends AbstractOutputDAO<PictureOutputVO> {
	public AbstractPictureOutputDAO(ServletContext context) {
		super(context);
	}

	public AbstractPictureOutputDAO(String configPath) {
		super(configPath);
	}
}
