package fudan.mcd.dao.abs;

import javax.servlet.ServletContext;

import fudan.mcd.vo.EnumOutputVO;

public abstract class AbstractEnumOutputDAO extends AbstractOutputDAO<EnumOutputVO> {
	public AbstractEnumOutputDAO(ServletContext context) {
		super(context);
	}

	public AbstractEnumOutputDAO(String configPath) {
		super(configPath);
	}
}
