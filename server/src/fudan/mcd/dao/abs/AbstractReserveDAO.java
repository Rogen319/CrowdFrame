package fudan.mcd.dao.abs;

import javax.servlet.ServletContext;

import fudan.mcd.vo.ReserveVO;

public abstract class AbstractReserveDAO extends AbstractDAO<Integer, ReserveVO> {
	public AbstractReserveDAO(ServletContext context) {
		super(context);
	}

	public AbstractReserveDAO(String configPath) {
		super(configPath);
	}
}
